// Get DOM elements
const emailField = document.getElementById('emailField')
const passwordField = document.getElementById('passwordField')
const firstnameField = document.getElementById('firstnameField')
const lastnameField = document.getElementById('lastnameField')
const weightField = document.getElementById('weightField')

const loginButton = document.getElementById('loginButton')
const registerButton = document.getElementById('registerButton')


// Add login event
registerButton.addEventListener('click', e => {
    const email = emailField.value
    const password = passwordField.value

    firebase.auth().createUserWithEmailAndPassword(email, password).catch(function(e) {
      console.log(e.code)
      console.log(e.message)
    })

})

loginButton.addEventListener('click', e => {
  window.location = './index.html'
})

firebase.auth().onAuthStateChanged(firebaseUser => {
    if(firebaseUser){
      console.log(firebaseUser)
      var z = new Date()
      var y = z.getFullYear()
      var m = z.getMonth()
      var d = z.getDate()

      const weight = weightField.value
      const firstname = firstnameField.value
      const lastname = lastnameField.value

      var name = {
        'first': firstname,
        'last': lastname
      }
      console.log(name)
      var start = {
        'date'  : d,
        'month' : m,
        'year'  : y,
        'weight': weight
      }
      var units = {
        'abv'   : 'lbs.',
        'full'  : 'pounds'
      }

      uid = firebaseUser['uid']
      console.log(uid)

      var database = firebase.database()

      firebase.database().ref('users/' + uid).set({
        name: name,
        start: start,
        units: units
      }).then(function(){
        firebase.database().ref('weights/' + uid  + '/' + y + '/' + m + '/' + d).set({
          weight: weight
        }).then(function(){
          window.location = './dashboard'
        })
      })

    }
    else{
        console.log("not logged in")
    }
})
