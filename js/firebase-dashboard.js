// Get DOM elements
const logoutButton = document.getElementById('logoutButton')
const nameLabel = document.getElementById('nameLabel')
const nameTitle = document.getElementById('nameTitle')
const dateLabel = document.getElementById('dateLabel')
const mainDateLabel = document.getElementById('mainDateLabel')

const weightInput = document.getElementById('weightInput')
const weightPromptLabel = document.getElementById('weightPromptLabel')
const weightPromptLabelHelper = document.getElementById('weightPromptLabelHelper')
const weightHelperTextMessage = document.getElementById('weightHelperTextMessage')
const saveWeightButton = document.getElementById('saveWeightButton')
const changeDateButton = document.getElementById('changeDateButton')

const latestWeight = document.getElementById('latestWeight')
const percentChanged = document.getElementById('percentChanged')
const weightChanged = document.getElementById('weightChanged')

const startWeight = document.getElementById('startWeight')
const percentChangedStart = document.getElementById('percentChangedStart')
const weightChangedStart = document.getElementById('weightChangedStart')

const successMessage = document.getElementById('successMessage')
const errorMessage = document.getElementById('errorMessage')

var CURRENT_WEIGHT = 0.0
var START_WEIGHT = 0.0
var FIREBASE_USER = ''
var UID = ''
var MONTH_IDX = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
var DAY_IDX = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]

// Group Functionality
const groupHelperAlert = document.getElementById('groupHelperAlert') // Whole Group Alert
const groupHelperText = document.getElementById('groupHelperText') // Text for Group alert

const createGroupButton = document.getElementById('createGroupButton')
const createGroupID = document.getElementById('createGroupID')
const createGroupName = document.getElementById('createGroupName')
const joinGroupButton = document.getElementById('joinGroupButton')
const joinGroupID = document.getElementById('joinGroupID')

createGroupButton.addEventListener('click', e => {
  createGroupID.value = ''
  createGroupName.value = ''
})

joinGroupButton.addEventListener('click', e => {
  joinGroupID.value = ''
})

const createGroupModalButton = document.getElementById('createGroupModalButton')
const joinGroupModalButton = document.getElementById('joinGroupModalButton')

createGroupModalButton.addEventListener('click', e => {
  groupHelperAlert.style="display:none"

  gid = createGroupID.value
  gname = createGroupName.value

  if(gname.length < 5){
    raiseCreateGroupError(1, gid, gname)
    return
  }
  if(gid.length < 5){
    raiseCreateGroupError(0, gid, gname)
    return
  }

  var database = firebase.database()
  firebase.database().ref('/users/' + UID).once('value').then(function(snapshotUser) {
    currentUser = snapshotUser.val()
    canCreateGroup = true
    if(currentUser['group']['owner'].length > 4){
      console.log("owner of too many groups")
      canCreateGroup = false
      return
    }
  }).then(function(){
    if(!canCreateGroup){
      raiseCreateGroupError(5, gid, gname)
    }
    console.log("still continuing...")
    firebase.database().ref('/groups/').once('value').then(function(snapshotGroup) {
      groupList = snapshotGroup.val()
      if(gid in groupList){
        raiseCreateGroupError(-1, gid, gname)
        return
      }
      // Create the Group
      firebase.database().ref('/groups/' + gid).set({
        name: gname,
        owner: UID
      }).then(function(){
        resetGroupPage(UID)
      })
    })
  })
})

joinGroupModalButton.addEventListener('click', e => {
  groupHelperAlert.style="display:none"

  gid = joinGroupID.value

  if(gid.length < 5){
    raiseCreateGroupError(0, gid, gname)
    return
  }

  var database = firebase.database()
  firebase.database().ref('/users/' + UID).once('value').then(function(snapshotUser) {
    currentUser = snapshotUser.val()
    console.log(gid)
    console.log(currentUser)
    console.log(currentUser['group']['member'])
    userGroups = currentUser['group']['member']
    if(userGroups.includes(gid)){
      raiseCreateGroupError(6, gid)
      canJoinGroup = false
      return
    }
  }).then(function(){
    if(!canJoinGroup){
      return
    }
    firebase.database().ref('/groups/').once('value').then(function(snapshotGroup) {
      groupList = snapshotGroup.val()
      if(!(gid in groupList)){
        raiseCreateGroupError(-2, gid)
        return
      }
      groupMembers = []
      if('members' in groupList[gid]){
        groupMembers = groupList[gid]['members']
      }
      groupMembers.push(UID)
      // Add User to Group
      firebase.database().ref('/groups/' + gid).set({
        members: groupMembers,
      }).then(function(){
        userGroups.append(gid)
        firebase.database().ref('/users/' + UID + "group").set({
          members: userGroups,
        }).then(function(){
          resetGroupPage(UID)
        })
      })
    })
  })
})


function raiseCreateGroupError(errorCode, gid="", gname=""){
  groupHelperAlert.style = "display:block"
  if(errorCode == 5){
    groupHelperAlert.classList.remove("alert-info")
    groupHelperAlert.classList.add("alert-danger")
    groupHelperText.innerText = "You are already the owner of 5 groups. You cannot create another group unless you delete an existing group."
  }
  if(errorCode == 6){
    groupHelperAlert.classList.remove("alert-info")
    groupHelperAlert.classList.add("alert-success")
    groupHelperText.innerText = "You are already a member of this group."
  }
  if(errorCode == -1){
    groupHelperAlert.classList.remove("alert-info")
    groupHelperAlert.classList.add("alert-danger")
    groupHelperText.innerText = "Please choose a Group ID different than " + gid + "."
  }
  if(errorCode == -2){
    groupHelperAlert.classList.remove("alert-info")
    groupHelperAlert.classList.add("alert-danger")
    groupHelperText.innerText = "A group with ID of \"" + gid + "\" does not exist."
  }
  if(errorCode == 0){
    groupHelperAlert.classList.remove("alert-info")
    groupHelperAlert.classList.add("alert-danger")
    groupHelperText.innerText = "Your Group ID of \"" + gid + "\" must be longer than 5 characters."
  }
  if(errorCode == 1){
    groupHelperAlert.classList.remove("alert-info")
    groupHelperAlert.classList.add("alert-danger")
    console.log(gid)
    console.log(gname)
    groupHelperText.innerText = "Your Group Name of \"" + gname + "\" must be longer than 5 characters."
  }
}

function resetGroupPage(UID){
  return
}


// Popup Update Weight
const addWeightButton = document.getElementById('addWeightButton')
const addWeightInput = document.getElementById('addWeightInput')
const monthDropdown = document.getElementById('monthDropdown')
const monthDropdownButton = document.getElementById('monthDropdownButton')
const yearDropdown = document.getElementById('yearDropdown')
const yearDropdownButton = document.getElementById('yearDropdownButton')
const addDayInput = document.getElementById('addDayInput')

const modalDate = document.getElementById('modalDate')

var monthDropdownIndex
var yearDropdownIndex

changeDateButton.addEventListener('click', e => {
  var z = new Date()
  var y = z.getFullYear()
  var m = z.getMonth()
  var d = z.getDate()
  console.log(m, d, y)
  addDayInput.value = d
  addWeightInput.value = ''
  monthDropdownIndex = m
  monthDropdownButton.innerText = MONTH_IDX[m]
  yearDropdownIndex = y
  yearDropdownButton.innerText = y
})

const monthDropdowns = document.getElementsByClassName('month-dropdown')
for(i = 0; i < monthDropdowns.length; i++){
  monthDropdowns[i].style.color = "#507A8E"
  monthDropdowns[i].addEventListener('click', e =>{
    monthDropdownIndex = e.target.getAttribute("value")
    monthDropdownButton.innerText = e.target.innerText
    console.log(monthDropdownIndex, monthDropdownButton.innerText)
  })
}

const yearDropdowns = document.getElementsByClassName('year-dropdown')
for(i = 0; i < yearDropdowns.length; i++){
  yearDropdowns[i].style.color = "#507A8E"
  yearDropdowns[i].addEventListener('click', e =>{
    yearDropdownIndex = e.target.getAttribute("value")
    yearDropdownButton.innerText = e.target.innerText
  })
}

addWeightButton.addEventListener('click', e => {
  if(addDayInput.value > 0 && addDayInput.value < 32){
    weight = parseFloat(addWeightInput.value).toFixed(1)
    y = yearDropdownIndex
    m = monthDropdownIndex
    d = addDayInput.value
    var database = firebase.database()
    firebase.database().ref('weights/' + UID  + '/' + y + '/' + m + '/' + d).set({
      weight: weight
    }).then(function(){
      resetDashboard(UID)
    })
  }
  else{
    return
  }
})

saveWeightButton.addEventListener('click', e => {
  saveWeightButton.innerHTML = '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>'
  saveWeightButton.disabled = true

  var z = new Date()
  var y = z.getFullYear()
  var m = z.getMonth()
  var d = z.getDate()
  var weight = weightInput.value
  if(weight && weight != CURRENT_WEIGHT){
    console.log(weight)
    console.log(CURRENT_WEIGHT)
    console.log("Updating weight in DB")
    var database = firebase.database()
    firebase.database().ref('weights/' + UID  + '/' + y + '/' + m + '/' + d).set({
      weight: weight
    }).then(function(){
      resetDashboard(UID)
    })
  }
  const delay = t => new Promise(resolve => setTimeout(resolve, t));

  successMessage.style = "display:block"
  delay(2000).then(() => {
    successMessage.style = "display:none"
    saveWeightButton.innerText = "Update"
    saveWeightButton.disabled = false
  })

})

const mainWeightTable = document.getElementById('mainWeightTable')
function createMainTable(weights){
  // console.log(weights) ALL WEIGHTS DATA
  mainWeightTable.innerHTML = '<table class="table table-striped" id="mainWeightTable"><thead><tr><th class="main-title-color" scope="col">Day</th><th class="main-title-color" scope="col">Weight</th></tr></thead><tbody></tbody></table>'

  //mainWeightTable.remove()
  var colorRow = true

  weights_r = []
  for(year in weights){
    for(month in weights[year]){
      for(day in weights[year][month]){
        data = {}
        data['w'] = weights[year][month][day]['weight']
        data['m'] = month
        data['d'] = day
        data['y'] = year
        weights_r.unshift(data)
      }
    }
  }

  for(data in weights_r){
    z = weights_r[data]
    w = parseFloat(z['w'])
    m = z['m']
    d = z['d']
    y = z['y']
    var wRow = document.createElement('tr')
    var date = document.createElement('td')
    date.classList.add("sidekick-title-color")
    date.innerHTML = MONTH_IDX[m] + ' ' + d + getDayEnding(d) + ', ' + y
    var weight = document.createElement('td')
    weight.classList.add("sidekick-title-color")
    weight.innerHTML = w.toFixed(1)
    wRow.appendChild(date)
    wRow.appendChild(weight)

    if(colorRow){
      wRow.style = "background: aliceblue"
    }
    colorRow = !colorRow
    //console.log(month + '/' + day + '/' + year + ': ' + w)
    mainWeightTable.append(wRow)
  }
}

function resetDashboard(uid){
  setupDashboard(uid)
}
function setupDashboard(uid){
  // console.log(uid)
  // Get a reference to the database service
  var database = firebase.database()
  firebase.database().ref('/users/' + uid).once('value').then(function(snapshot) {
    var user = snapshot.val()
    console.log(user)
    START_WEIGHT = user['start']['weight']
    UNITS = user['units']
    updateName(user['name'])
    updateStart(user['start'])
  })

  var z = new Date()
  var y = z.getFullYear()
  var m = z.getMonth()
  var d = z.getDate()
  var a = z.getDay()
  updateDate(m, d, a, y)

  firebase.database().ref('/weights/' + uid).once('value').then(function(snapshot) {
    weights = snapshot.val()
    CURRENT_WEIGHT = null
    try{
      CURRENT_WEIGHT = weights[y][m][d]['weight']
    }
    catch{

    }
    weightInput.value = CURRENT_WEIGHT
    lastW = findLatestWeight(weights, y, m, d)
    latestWeight.innerText = lastW + ' ' + UNITS['abv']
    if(CURRENT_WEIGHT){
      console.log("Already logged a weight today...")
      weightPromptLabel.innerText = "Today's Weight"
      weightPromptLabelHelper.innerText = "Your weight for today has already been entered. Type a new number and press update to change today's weight."
      weightHelperTextMessage.classList.remove('alert-danger')
      weightHelperTextMessage.classList.add('alert-success')
      weightHelperTextMessage.style = "display:block"
      saveWeightButton.innerText = "Update"
      changePercent = (100 * (CURRENT_WEIGHT - lastW) / lastW).toFixed(2)
      changePercentStart = (100 * (CURRENT_WEIGHT - START_WEIGHT) / START_WEIGHT).toFixed(2)
      changeWeight = (CURRENT_WEIGHT - lastW).toFixed(2)
      changeWeightStart = (CURRENT_WEIGHT - START_WEIGHT).toFixed(2)
      if(changePercent > 0){
        percentChanged.innerText =  "+" + changePercent + "%"
        weightChanged.innerText =  "+" + changeWeight + ' ' + UNITS['abv']
      }
      else if (changePercent < 0) {
        percentChanged.innerText =  changePercent + "%"
        weightChanged.innerText =  changeWeight + ' ' + UNITS['abv']
      }
      else{
        percentChanged.innerText =  0 + "%"
        weightChanged.innerText =  0 + ' ' + UNITS['abv']
      }
    }
    else{
      changePercentStart = (100 * (lastW - START_WEIGHT) / START_WEIGHT).toFixed(2)
      changeWeightStart = (lastW - START_WEIGHT).toFixed(2)
      console.log("No weight logged today...")
      weightPromptLabel.innerText = "Enter Today's Weight"
      weightPromptLabelHelper.innerText = "Your weight for today has not yet been entered. Type a number and press enter to add your weight."
      weightHelperTextMessage.classList.remove('alert-success')
      weightHelperTextMessage.classList.add('alert-danger')
      weightHelperTextMessage.style = "display:block"
      saveWeightButton.innerText = "Enter"
      percentChanged.innerText = "0%"
      percentChangedStart.innerText =  "0%"
      weightChanged.innerText = 0 + ' ' + UNITS['abv']
      weightChangedStart.innerText =  0 + ' ' + UNITS['abv']
    }

    if(changePercentStart > 0){
      percentChangedStart.innerText =  "+" + changePercentStart + "%"
      weightChangedStart.innerText =  "+" + changeWeightStart + ' ' + UNITS['abv']
    }
    else if(changePercentStart < 0){
      percentChangedStart.innerText =  changePercentStart + "%"
      weightChangedStart.innerText =  changeWeightStart + ' ' + UNITS['abv']
    }
    else{
      percentChangedStart.innerText =  0 + "%"
      weightChangedStart.innerText =  0 + ' ' + UNITS['abv']
    }
  })
  firebase.database().ref('/weights/' + uid).once('value').then(function(snapshot) {
    all_weights = snapshot.val()
    createMainTable(all_weights)
    num = countWeights(all_weights)
    console.log("Logged Weights: " + num)
  })
}

function findLatestWeight(weights, y, m, d){
  w = -1
  while(w < 0 && y != 2019){
    if(d == 1){
      d = 31
      if(m == 0){
        m = 11
        y -= 1
      }
      else{
        m -= 1
      }
    }
    else{
      d -= 1
    }
    try{
      w = weights[y][m][d]['weight']
    }
    catch{
      // keep looking...
    }
  }
  if(w == -1){
    return "No Weights"
  }
  return w
}

function countWeights(w){
  weight = 0
  for(y in w){
    for(m in w[y]){
      for(d in w[y][m]){
        for(z in w[y][m][d]){
          weight += 1
        }
      }
    }
  }
  return weight
}

function updateDate(m,d,w,y){
  dateLabel.innerText = DAY_IDX[w] + ', ' + MONTH_IDX[m] + ' ' + d + getDayEnding(d) + ', ' + y
  mainDateLabel.innerText = DAY_IDX[w] + ', ' + MONTH_IDX[m] + ' ' + d + getDayEnding(d) + ', ' + y
  modalDate.innerText = DAY_IDX[w] + ', ' + MONTH_IDX[m] + ' ' + d + getDayEnding(d) + ', ' + y
}

function updateStart(s){
  startWeight.innerText = s['weight'] + ' ' + UNITS['abv']
}

function updateName(n){
  nameLabel.innerText = n['first'] + ' ' + n['last']
  nameTitle.innerText = n['first'] + "'s Weight Dashboard"
}

function getDayEnding(d){
  ending = ''
  if(d == 1 || d == 21 || d == 31){
    ending = 'st'
  }
  else if(d == 2 || d == 22){
    ending = 'nd'
  }
  else if(d == 3 || d == 23){
    ending = 'rd'
  }
  else{
    ending = 'th'
  }
  return ending
}

// Add section changing code
const dashboardButton = document.getElementById('dashboardButton')
const settingsButton = document.getElementById('settingsButton')
const groupButton = document.getElementById('groupButton')
const dashboardSection = document.getElementById('dashboardSection')
const settingsSection = document.getElementById('settingsSection')
const groupSection = document.getElementById('groupSection')

// Section button switching logic
dashboardButton.addEventListener('click', e => {
    dashboardSection.style = "display:block"
    dashboardButton.classList.remove("sidebar-button-color-notselected")
    dashboardButton.classList.add("sidebar-button-color-selected")
    settingsSection.style = "display:none"
    settingsButton.classList.remove("sidebar-button-color-selected")
    settingsButton.classList.add("sidebar-button-color-notselected")
    groupSection.style = "display:none"
    groupButton.classList.remove("sidebar-button-color-selected")
    groupButton.classList.add("sidebar-button-color-notselected")
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
})
settingsButton.addEventListener('click', e => {
    dashboardSection.style = "display:none"
    dashboardButton.classList.remove("sidebar-button-color-selected")
    dashboardButton.classList.add("sidebar-button-color-notselected")
    settingsSection.style = "display:block"
    settingsButton.classList.remove("sidebar-button-color-notselected")
    settingsButton.classList.add("sidebar-button-color-selected")
    groupSection.style = "display:none"
    groupButton.classList.remove("sidebar-button-color-selected")
    groupButton.classList.add("sidebar-button-color-notselected")
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
})
groupButton.addEventListener('click', e => {
    dashboardSection.style = "display:none"
    dashboardButton.classList.remove("sidebar-button-color-selected")
    dashboardButton.classList.add("sidebar-button-color-notselected")
    settingsSection.style = "display:none"
    settingsButton.classList.remove("sidebar-button-color-selected")
    settingsButton.classList.add("sidebar-button-color-notselected")
    groupSection.style = "display:block"
    groupButton.classList.remove("sidebar-button-color-notselected")
    groupButton.classList.add("sidebar-button-color-selected")
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
})

firebase.auth().onAuthStateChanged(firebaseUser => {
    if(firebaseUser){
        // console.log(firebaseUser)
        FIREBASE_USER = firebaseUser
        UID = firebaseUser['uid']
        setupDashboard(firebaseUser['uid'])
        setupSettings(firebaseUser['uid'])
    }
    else{
        console.log("not logged in")
        window.location = '..'
    }
})

// Add logout event
logoutButton.addEventListener('click', e => {
    const promise = firebase.auth().signOut()
    promise.catch(e => {
        console.log(e.code)
        console.log(e.message)
    })
})

const personNameInput = document.getElementById('personNameInput')
function setupSettings(uid){
  var database = firebase.database()
  firebase.database().ref('/users/' + uid).once('value').then(function(snapshot) {
    var user = snapshot.val()
    var name = (user['name'])
    personNameInput.value = name['first'] + ' ' + name['last']
    addressInput.value = FIREBASE_USER['email']
  })
}
